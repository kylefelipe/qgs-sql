# Como contribuir

No mundo _open source_ é muito comum a comunidade se ajudar, fazendo contribuições como proposição de novas funções, reportando erros, traduções e etc.  
Aqui vão algumas regras e dicas para contribuir para a melhoria do estudo.  

## Abra _issues_

Issues são o local ideal para relatar problemas, solicitar novidades. Principalmente quando usamos plataformas como [GITHUB](https://github.com) e [GITLAB](https://gilab.com).  
Elas facilitam a organização das ações, assim, quando alguma modificação for feita no projeto relativo à issue aberta é possível citar a mesma.

Lembre-se de citar onde está o problema relatado (se possível até com o link para a página/capitulo/sub capítulo), ajuda muito na hora de resolver.

Também é possível gerar boas discussões em torno do tema da issue, pois há a possibilidade de fazer comentários.

Para organizar ainda mais, é possível adicionar rótulos às issues, para poder ficar mais fácil a identificação da mesma.

Para criar uma issue, basta clicar em `issues` (__1__) e ir depois em _\<Nova Issue> (__2__).  

![Criando issue](../qgis_sql/img/0008_abrindo_issue.png)

Agora basta dar um título, adicionar uma descrição, atribuir a issue à você (`Assign to me`) e escolher a label adequada.

![Editando os dados da issue](../qgis_sql/img/0009_dados_issue.png)

Em seguida, clique em __\<Create issue>__.

## Faça Merge Request

Outra forma de fazer contribuições é fazendo _merge request_.  
É possível fazer um _fork_ (cópia) desse repositório, fazer as suas modificações e enviá-las para serem avaliadas e receberem o aceite ou não.  
Muitas pessoas pedem acesso para alteração diretamente do repositório, e isso não é uma boa prática, pois alterações diretas, feitas por terceiros necessitam de análise e aprovação.

## Via redes sociais

Na certa iremos nos esbarrar em alguma rede social por aí, seja ela o Telegram, Whatsapp, Instagram, Twitter, entre outros.  
O as sugestões serão bem vindas, mas o contato com esse tipo de contribuição é um pouco mais morosa, pois eu terei de abrir uma issue, citar a sua contribuição...  
Se possível, abra uma issue...

## Mande e-mail

Por favor, não mande e-mail, abra uma issue.
Eu recebo muuuuitos e-mails diáriamente e o seu poderá se perder no meio deles.
