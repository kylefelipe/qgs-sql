# Operações Básicas em SQL

Sempre precisamos de fazer alguma operação em algum lugar, e no SQL não seria diferente disso.  
A linguagem possui instrumentos para realizamos as mais diversas operações que precisamos, como soma, multiplicação, divisão, e outras.
Para as funções matemáticas, podemos encontrar uma lista completa delas [aqui](https://www.sqlite.org/lang_mathfunc.html) e nas [funções de agregações também](https://www.sqlite.org/lang_aggfunc.html).

## Somando

Podemos usar o operador `+` para fazer somas de valores no [SQLITE](https://www.sqlite.org).

```sql
SELECT 1 + 1;
```

![Soma em SQL](../img/0024_soma.png)

também podemos envolver campos na soma.

```sql
SELECT id, 1 + id  as id_alterado
FROM lml_unidade_federacao_a
ORDER BY id
LIMIT 5;
```

![Soma com campos em SQL](../img/0025_soma_com_campos.png)

## Sutraindo

Com o operador `-` podemos realizar subtrações.

```sql
SELECT 43 - 1;
```

## Multiplicando

Usamos o operador `*` para fazer multiplicações.

```sql
SELECT 3 * 2;
```

## Dividindo

Com o operador `-` podemos realizar divisões.

```sql
SELECT 100 / 2;
```

Tente aplicar alguma coluna a essas operações anteriores.

O [SQLITE](https://www.sqlite.org) por padrão não vem com algumas operações, como potenciação e radiciação, para ter acesso a elas, é necessário fazer uma compilação própria, ou criar uma função (que veremos mais à frente.).
