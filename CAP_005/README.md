# Tipos de dados

Trabalhamos com dados o tempo todo.  
Baixamos dados do IBGE, buscamos dados em pesquisas no Google, baixamos dados do OSM - Open Street Map.

Normalmente esses dados são do tipo texto, decimal, inteiro, mas, temos vários outros tipos que podem nos ajudar muito a trabalhar nossas informações.

Cada tipo de dado tem seu comportamento específico, funções e até exibição próprias.

O [SQLITE](https://www.sqlite.org) possui cinco tipos de dados primitivos, que são conhecidos como _classes de armazenameno_ (_storage classes_).

Para ver mais sobre tipos de dados no [SQLITE](https://www.sqlite.org) [clique aqui](https://www.sqlite.org/datatype3.html), lá você verá que os tipos de dados possuem diversos outros sub-tipos que podemos usar no dia a dia.

Temos 5 tipos de dados básicos no [SQLITE](https://www.sqlite.org):

- `NULL` Para valor nulo.
- `INTEGER` Para valores inteiros.
- `REAL` Para numeros não inteiros
- [`TEXT`](./README.md#text) Para valores do tipo texto (cadeia de texto, A.K.A text string), que são armazenadas no seguinte encoding ([UTF-8](https://developer.mozilla.org/pt-BR/docs/Glossary/UTF-8), UTF-16BE ou UTF-16LE )
- `BLOB` (Big Large Object), É uma forma de guardar dados da forma que chegam ao banco (por exemplo, um PDF seria armazenado como BLOB bem como uma imagem ou um arquivo executável)

## Tipagem de dados dinânmica

Em bancos de dados como o [PostgreSQL](https://www.postgresql.org), [MySQL](https://www.mysql.com) trabalhamos com o que chamamos de `tipagem estática`.

Ou seja, determinamos o tipo de dado que a coluna pode receber quando a criamos e valores de tipos diferentes não podem ser inseridos.

Já no [SQLITE](https://www.sqlite.org) as coisas funcionam um pouquinho diferentes, por padrão, podemos até declarar o tipo do dado a ser inserido no campo, mas ele irá aceitar a inserção de outros tipos no campo, pois ele utiliza a forma de `tipagem dinâmica` dos dados dentro dele.
E o que vai determinar o seu tipo é o valor armazenado, não a coluna

Para verificarmos o tipo, podemos usar a função `typeof(dado_ou_coluna)`.

> Mas Kyle, então cada tabela do SQLite é como se fosse uma planilha, só que cabe mais coisas.  
> Exatamente.  
> Isso pode ser um problema.  
> Pode sim.  
> E como evitar isso??  
> "Bora vê então.."

### STRICT

Caso haja o desejo de travar os tipos de dados nos campos (_static typing_) devemos utilizar a palavra reservada `STRICT` logo após a definição dos campo da tabela, durante a sua criação.  
Nesse caso, todos os campos devem ter o tipo de dados definido na sua criação.

```sql
-- Dynamic Typing - o valor armazenado determina seu tipo

CREATE TABLE tabela_de_tipos_dinamicos (
  id INTEGER PRIMARY KEY AUTOINCREMENTAL,
  campo_sem_tipo,
  campo_texto TEXT,
  campo_real real,
  campo_inteiro integer
);

-- Static Typing - tipo da coluna determina os tipos armazenados

CREATE TABLE tabela_de_tipos_estaticos (
  id INTEGER PRIMARY KEY AUTOINCREMENTAL,
  campo_blob blob,
  campo_texto TEXT,
  campo_real real,
  campo_inteiro integer
) STRICT; -- Restringe o tipo de dado ao tipo da coluna
```

Vamos fazer algunas inserções para verificar:

- Tabela com tipagem dinâmica:

```sql
INSERT INTO tabela_de_tipos_dinamicos (
  campo_sem_tipo, campo_texto, campo_real, campo_inteiro
)
VALUES
  ('aqui os tipos vao bater com o campo', 'sou um campo de texto', 3.14, 42),
  ('Aqui os tipos serão diferentes', 777, 42,  3.14),
  (42, NULL, 'caraca que loucura', x'0011'); -- o x no ultimo valor indica que é um BLOB

SELECT id,
  campo_sem_tipo, typeof(campo_sem_tipo) AS tipo_do_dado,
  campo_texto,typeof(campo_texto) AS tipo_do_dado,
  campo_real, typeof(campo_real) AS tipo_do_dado,
  campo_inteiro, typeof(campo_inteiro) AS tipo_do_dado
FROM tabela_de_tipos_dinamicos;
```

![Select de dados dinâmicos](../img/0026_select_tipo_dado_dinamico.png)

- Tabela com tipagem estática

```sql
INSERT INTO tabela_de_tipos_estaticos (
  campo_blob, campo_texto, campo_real, campo_inteiro
)
VALUES
  (x'0011', 'nessa linha vai ser ok', 3.14, 42),
  ('vai dar ruim', 777, 42,  3.14),
  (42, NULL, 'caraca que loucura', x'0011');
```

![Insert em tabela de tipo estática](../img/0027_insert_tipo_dado_estatico.png)

Com esses exemplos vimos que podemos mudar o comportamento do SQLite quanto ao uso da _tipagem dinânica_ e _tipagem estática_, ao tentar inserir um valor que não corresponde ao tipo da coluna o banco vai acusar um erro.

## Boolean

[SQLITE](https://www.sqlite.org) não oferece um tipo específico para booleanos, nesse caso os valores são armazenados como inteiros (integers), sendo 0 (falso) e  1 (verdadeiro).
A partir da versão 3.23 as palavras chaves `TRUE` e `FALSE` são reconhecidas, mas são apenas uma alternativa ao uso do 0 e 1 literais, os valores continuam sendo salvos na mesma forma.

## Text

Text é uma forma de guardarmos texto dentro de um banco de dados.

Com com o tipo text, é possível realizar várias operações relativas a texto, é possível unir um texto com outro (concatenar), pegar apenas um pedaço de um texto (substring) buscar padrões dentro do texto (REGEX, Like).

E uma curiosidade do [SQLITE](https://www.sqlite.org), por não possuir um formato específico para data e hora, [esse é um formato que é utilizado para isso](https://www.sqlite.org/lang_datefunc.html).

O interessante do formato `text` é que é possível também emular outros formatos. Você certamente já trabalhou com um CSV em algum momento.  
O CSV - Comma Separated-Values (Valores Separados por Vírgula) é um arquivo de texto que obedeçe um certo padrão para emular outros dados, é um TEXTO DELIMITADO, e nesse caso é um arquivo onde as vírgulas são separadores de valores.  
O famoso arquivo JSON - JavaScript Object Notation, também é um formato de dado que é representado por uma string com seus valores dentro seguindo um determinado padrão, e dentro dele é possível guardar dados primitivos e estruturas de dados (veremos sobre mais tarde).  

Vá no [Qgis](www.qgis.org), execute o seguinte SQL e veja o resultado da consulta:

```sql
SELECT 'a resposta é' + 42;
-- resultado: 42
```

O [SQLITE](https://www.sqlite.org) irá avaliar se é possível transformar a string em algum formato numérico e tentar realizar a operação solicitada, como não cosegue, vai considerar que o valor é 0.

Agora tente o seguinte:

```sql
SELECT 'A resposta é ' || 42;  
-- resultado: A resposta é 42
```

O número 42 será concatenado à string, pois essa é uma forma de concatenar strings.

Agora teste o seguinte script:

```sql
SELECT '12' + '30';  
-- resultado: 42
```

Isso acontece devido ao fato de o [SQLITE](https://www.sqlite.org) tenta transformar as strings em um formato apropriado (nesse caso ambas as strings são inteiras), e realiza a operação.

Tente fazer a soma de dois textos:

```sql
SELECT 'Super amigos ' + 'ativar';  
-- resultado: 0
```

## Qualidade de dados

Esse é um tema que gosto bastante de falar, pois nos grupos onde participo, nos locais onde já trabalhei é algo que causa muita dor de cabeça, principalmente para iniciantes nas artes do geoprocessamento.

Então fique atento às [boas práticas](../CAP_002/README.md#boas-práticas) já citadas anteriormente. Fica aqui a de um artigo no meu blog sobre o assunto [Dados x Qualidade](https://kylefelipe.com/blog/2018/05/28/dados-x-qualidade-dores-de-cabeca-evitaveis-ao-trabalhar-e-produzir-dados/), vale a pena imprimir e colocar num local bem visível e com letras garrafais para consulta sempre que tiver um problema.
