# Criando bancos no QGIS

A partir de agora é interessante termos um arquivo para podermos trabalhar alguns comandos SQL.

Vamos precisar de criar tabelas, inserir dados, remover dados, e trabalhar apenas com as camadas que estão no projeto como base de dados vai ser um problema, principalmente pelo motivo de não ser possível criar camadas usando o comando `CREATE TABLE` nesse tipo de conexão.

Vale a pena relembrar que o [QGIS](www.qgis.org) apenas consegue acessar bancos de dados que possuam a parte espacial ativa, e aqui vamos criar um banco [SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) para utilizar.

Já citei o [SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) na [introdução](../INTRODUCAO.md#uma-breve-introdução).

A vantagem é que o [SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) é utilizado para extender a sintaxe do [SQLITE](https://www.sqlite.org) para trabalharmos com dados geográficos (o [SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) está para o [SQLITE](https://www.sqlite.org) como o [Postgis](https://postgis.net) para o [PostgreSQL](https://www.postgresql.org)), sendo possível usar nas consultas das camadas que estão adicionadas no projeto ou no próprio banco [SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) e até mesmo em um [Geopackage](http://www.geopackage.org/).

O interessante de usar o [SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) é a facilidade de trabalhar ele com [SQLite Browser](https://sqlitebrowser.org/), e a facilidade criar novas tabelas.

## Criando o primeiro banco

No [QGIS](www.qgis.org) vamos em `Camada > Criar Camada > Nova Camada Spatialite`

![Criando Spatialite](../img/0028_criando_spatialite.png)

Na tela que abrir vamos clicar no botão `...` para criar um novo arquivo

![](../img/0029_criando_arquivo.png)

Em seguida vamos dar um nome para o arquivo, a extensão de arquivos do banco [SQLITE](https://www.sqlite.org)/[SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) são `.db` ou `.sqlite`

![](../img/0030_nome_arquivo_db.png)

Nesse ponto o [QGIS](www.qgis.org) já criou o arquivo vazio, e o mesmo estará disponível na lista de conexções spatialite para conexão.

![](../img/0031_novo_banco_disponivel.png)

Para rodar algum comando SQL, basta selecionar o banco no painel de provedores ( 1 ), e em seguida clicar no botão `Janela SQL` (2).

![](../img/0032_db_manager.png)

- 3: Janela SQL indicando onde está conectada.
- 4: Caixa de texto onde vamos digitar os comandos.
- 5: Local onde será exibido mensagens de erro ou os resultados dos comandos SQL.

Para executar os comandos basta clicar no botão `<Executar>` ou usar a tecla de atalho `Ctrl + R`.
