# Onde usar SQL

Já vimos um pouquinho sobre os motivos de usar dbs, os principais bancos que temos hoje e qual a base que o [Qgis](www.qgis.org) utiliza. E agora vamos ve onde podemos usar a linguagem dentro do [Qgis](www.qgis.org).

Já foi falado que não é necessário a instalação de nenhum SGBD, pois o prório [Qgis](www.qgis.org) já possui uma ferramenta para isso. Falamos também que dentro do aquivo de projeto do [Qgis](www.qgis.org) há um banco [SQLITE](https://www.sqlite.org) que é utilizado por ele e alguns plugins, e que mais à frente podemos utilizar também.  

Mas o que não foi dito é que, não é necessário que uma camada esteja dentro de um bd para que possamos utilizar SQL com essa camada.  
Basta adicionar uma camada a um projeto (novo ou pré existente) para podermos trabalhar com elas.  
Isso é uma característica que o [Qgis](www.qgis.org) herda da [GDAL](https://gdal.org).  

## Gerenciador de banco de dados

O [Qgis](www.qgis.org) já possui em seu núcleo um gerenciador de banco de dados bem simples, o DB Manager.  
Para acessá-lo basta ir em `Base de dados > Gerenciador de BD`.

![Acessando o DB Manager](../img/0001_menu_base_de_dados.png)

Obs: Caso o menu não estiver aparecendo, basta aitvar o complemento `DB Manager` no `Gerenciador de complementos`.  

Dentro dele podemos rodar SQLs dentro dos bancos e nas camadas que foram adicionadas ao projeto do [Qgis](www.qgis.org).

![Db Manager](../img/0002_db_manager.png)

* 1 - Refresh (Atualizar): Esse botão permite pegar as atualizações de uma camada que ainda não refletiu nos dados adicionados ao [Qgis](www.qgis.org). Também funciona para bancos que tiveram novas camadas adicionadas ou camadas removidas.

* 2 - Janela SQL: Esse botão permiter abrir uma janela para criamos o sql dentro do painel esquerdo do gerenciador.

* 3 - Importar camada/arquivo: Esse botão permite inserir novas camadas de algum arquivo, ou outra camada no projeto, para um determinado banco.

* 4 - Exportar para arquivo: Permite fazer a operação inversa do botão anterior, pega uma camada e salva ela em um arquivo,

* 5 - Painel Provedores: Esse painel mostra todos os provedores que podemos utilizar e suas camadas, bastam que eles tenham suas conexões já cadastradas no [Qgis](www.qgis.org), básicamente, ele mostra no início quatro provedores, [Geopackage](http://www.geopackage.org/), [Postgis](https://postgis.net), [SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) e Camada Virtual.

* 6 - Nesse painel podemos ter acesso aos metadados dos provedores, da tabela selecionada, uma pré-visualização dos dados e da janela SQL, quando clicamos no botão.

## Caixa de ferramentas

O [Qgis](www.qgis.org) possui uma `caixa de ferramentas`, que possui diversos processos para podermos usar em nossos dados.  
Ela fica no menu `Processar > Caixa de Ferramentas`, podemos acessar praticamente todos os processos que o [Qgis](www.qgis.org) utiliza, e os processos criados pela maioria dos plugins, e também criar nossos próprios processos, através do modelador gráfico ou usando scripts [Python](www.python.org).  
> O canal [Geocast Brasil](https://www.youtube.com/geocastbrasil) tem uma série de lives sobre [PyQgis](https://qgis.org/pyqgis/master/), e um repositório com os dados das aulas no [GITLAB](https://gitlab.com/geocastbrasil/livepyqgis) das lives para consulta.

![Habilitando a caixa de ferramentas](../img/0004_habilitando_caixa_de_ferramentas.png)

Obs.: Caso não haja um menu `Processar` basta habilitar o plugin `Processing` no gerenciador de plugins.

No painel da caixa de ferramentas, temos caixa de ferramenta `Banco de dados`

![Caixa de ferramentas](../img/0005_caixa_de_ferramentas.png)

## Ao adicionar uma camada

Ao adicionarmos uma camada ao projeto é possível estabelecer filtros usando sql.  
No `Gerenciador de fonte de dados` escolhemos uma fonte (_1_).

![Gerenciador de Fonte de Dados](../img/0006_gerenciador_fonte_dados.png)  
>filtrando uma camada [Geopackage](http://www.geopackage.org/)

Escolhemos uma camada (_2_) e clicamos nela duas vezes e a janela irá abrir para montarmos uma consulta básica.

![Janela da Ferramenta de consulta](../img/0007_ferramenta_consulta.png)

Nela podemos montar uma consulta simples para aplicar como filtro na camada, e ao clicar em __\<OK>__ esse filtro irá aparecer na coluna `Sql` do gerenciador de fonte de dados (__3__).
