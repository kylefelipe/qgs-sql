# QGIS & SQL - Tudo a ver

Já ouviram falar de SQL?  
Aposto que sim, mas sabiam que é possível usar SQL dentro do [Qgis](www.qgis.org)?  
Provavelmente não.
E o melhor, nem precisa instalar NADA na máquina para aprender SQL se já tiver o [Qgis](www.qgis.org) instalado?  

Então, vamos embarcar nessa longa aventura e descobrir aos poucos o que podemos fazer e onde podemos aplicar, e perceber que não precisamos de plugins para fazermos algumas coisas.

__PS1__: Durante o estudo estarei usando termos normalmente utilizados no geoprocessamento, não se acanhem em perguntar caso não conheça.  
__PS2__: Os capítulos serão colocados aos poucos, na medida que forem escritos.  
__PS3__: Vamos utilizar os dados [do IBGE](https://www.ibge.gov.br/geociencias/downloads-geociencias.html), para fazer o download [clique aqui](https://geoftp.ibge.gov.br/cartas_e_mapas/bases_cartograficas_continuas/bc250/versao2019/geopackage/bc250_2019-10-29.gpkg).

Para fazer contribuições, leia a sessão de [contribuições](contributing.md) para fazer as propostas.

## Indice dos capitulos

[Introdução](INTRODUCAO.md)

[Contribuições](contributing.md)

1. [Capítulo 1 - Onde Usar](CAP_001/README.md)  
   1.1. [Gerenciador de Banco de dados](CAP_001/README.md#Gerenciador_de_banco_de_dados)  
   1.2. [Caixa de ferramentas](CAP_001/README.md#Caixa_de_ferramentas)  
   1.3. [Ao adicionar uma camada](CAP_001/README.md#Ao_adicionar_uma_camada)

2. [Capítulo 2 - SQL -  Básico](CAP_002/README.md)  
   2.1 [Boas Práticas](CAP_002/README.md#Boas_praticas)  
   2.2 [Primeiros Passos](CAP_002/README.md#Primeiros_passos)  
      * [SELECT](CAP_002/README.md#SELECT)  
      * [ALIAS](CAP_002/README.md#Definindo_apelidos_(ALIAS)_-_AS)  
      * [O coriinga](CAP_002/README.md#O_coringa)  
      * [Limitando retorno da consulta](CAP_002/README.md#limitando_retorn_da_consulta)  
      * [Ordenando a consulta](CAP_002/README.md#Ordenando_a_consulta)  
      * [Distinção de valores](CAP_002/README.md#Distinção_de_valores)  
      * [Contando valores](CAP_002/README.md#Contando_valores)  
      * [Agrupando valores](CAP_002/README.md#Agrupando_valores)  
      * [Concatenando campos](CAP_002/README.md#Concatenando_campos)

3. [Capítulo 3 - Operações básicas em SQL](CAP_003/README.md)  
   3.1 [Somando](CAP_003/README.md#Somando)  
   3.2 [Subtraindo](CAP_003/README.md#Subtraindo)  
   3.3 [Mutiplicando](CAP_003/README.md#Mutiplicando)  
   3.4 [Dividindo](CAP_003/README.md#Dividindo)  

4. [Criando bancos no QGIS](CAP_004/README.md)  
   1. [Criando o primeiro banco](CAP_004/README.md#criando-o-primeiro-banco)

5. [Capítulo 5 - Tipos de dados](CAP_005/README.md)  
   5.1 [Tipagem de dados dinãmica](CAP_005/README.md#tipagem-de-dados-dinânmica)  
   5.1.1 [STRICT](CAP_005/README.md#strict)  
   5.2 [Boolean](CAP_005/README.md#boolean)  
   5.3 [Text](CAP_005/README.md#text)  
   5.4 [Qualidade de dados](CAP_005/README.md#qualidade-de-dados)  
