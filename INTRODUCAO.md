## Uma breve introdução

> O que é SQL, onde é usado, como vive, do que se alimenta?

SQL é uma das linguagens de programação mais simples que existem, e uma das mais simples de se aprender.  
O termo SQL vem de `Structured Query Language` (Linguagem de Consulta Estruturada - tradução livre).

É uma linguagem que segue um certo padrão para ser utilizada, na grande maioria das vezes, em consultas em bancos de dados, _doravante denominados como `db`_, mas no [Qgis](www.qgis.org) as coisas funcionam um pouco diferente.

O funcionamento da linguagem é bem similar entre os dbs que a utilizam, e sim, há bancos de dados que não utilizam SQL para as consultas, os famosos `NOSQL (Not Only SQL)`, mas eles não serão motivo desse estudo.  
Tanto que há muitas _keywords_ que são compartilhadas entre os dbs, mesmo sendo produzidos por empresas/comunidades diferentes, que possuem comportamento parecidíssimo, e isso ajuda no momento de migrar de um banco para outro, pois com um conhecimento básico, é possível trabalhar em vários sistemas diferentes.  
Veremos essas _keywords_ mais a frente.

No mercado existem muitas opções de bancos de dados que podem ser utilizados, desde bancos proprietários, tais como `[Oracle](https://www.oracle.com/br/index.html)`, `[SQL SERVER](https://www.microsoft.com/pt-br/sql-server/sql-server-downloads)`, até bancos que são livres, como `[PostgreSQL](https://www.postgresql.org/)`, o queridinho da maioria dos _geoprocesseiros_, `[MySQL](https://www.mysql.com/)`, um dos mais famosos e utilizados em sistemas, sites e etc, `[MariaDB](https://mariadb.org/)`, que é uma "versão" do MySQL criada pela comunidade e o `[SQLITE](https://www.sqlite.org)`.  
Desse todos esses livres citados podem ser instalados livremente para serem utilizados, estudados, admirados, amados e por aí vai, a única exceção é o [SQLITE](https://www.sqlite.org) que não precisa de instalação.

Normalmente, como falamos antes, é necessário a instalação do banco em uma máquina (seja a própria máquia que trabalhamos ou uma outra máquina dedicada apenas para ele, A.K.A servidor). Já com o [SQLITE](https://www.sqlite.org) a coisa funciona um pouquinho diferente.  
Mas como assim?  
Diferente dos outros bancos citados acima, o [SQLITE](https://www.sqlite.org) é um banco que é formado por um arquivo único, que pode ser colocado em um pendrive, hd portátil, cartão de memória e ser levado para qualquer lugar.  
Essa é uma característica que permite que ele seja utilizado em muitas aplicações, como por exemplo, em apps de celular (como os sistemas android/ios), podendo ser utilizado como base para outros arquivos, como os arquivos de projeto do [Qgis](www.qgis.org) e o [Geopackage](http://www.geopackage.org/).

Um momento, está dizendo que o SQLITE é utilizado no arquivo de projeto do [Qgis](www.qgis.org)?  
Exatamente. e isso permite muitas coisas legais, como gerar rótulos dinâmicos, armazenar dados do projeto e por aí vai.  
E como assim ele é a base do arquivo [Geopackage](http://www.geopackage.org/)?
O formato [Geopackage](http://www.geopackage.org/), gpkg para os íntimos, é um banco de dados portátil, que armazena dados geográficos e tabulares e seus metadados, sendo uma grande vantagem em relação à utilização formato SHAPEFILE, muito difundido hoje dentre as pessoas que utilizam de geotecnologias no dia a dia.  
Infelizmente, até o momento da escrita desse estudo sobre SQL e [Qgis](www.qgis.org) só é possível utilizar SQL em um banco [SQLITE](https://www.sqlite.org) para trabalhar dados tabulares, mas, porém, contudo, toda via, entretanto, um italino supimpa agraciou o mundo com uma extensão para o [SQLITE](https://www.sqlite.org), conhecida como [SPATIALITE](https://www.gaia-gis.it/fossil/libspatialite/index) que permite a utilização de consultas espaciais dentro do [SQLITE](https://www.sqlite.org) também, e adivinha qual é a sintaxe que o [Qgis](www.qgis.org) usa??.

## Bancos de dados e gerenciadores de bancos de dados

Um banco de dados é apenas um software que um sistema acessa para guardar, recuperar, modificar um dado. E em determinados momentos, precisamos de acessar esse banco para fazer alguma intervenção.

Alterar um dado colocado errado, criar uma tabela, criar um gatilho para algum evento dentro do banco, e muitas outras coisas, inclusive, para fazer alguma análise ou gerar algum relatório.

E isso só é possível de fazermos se tivermos um `SGBD` - Sistema Gerenciador de Banco de Dados.  
Com eles é possível acessar ao banco através de uma interface gráfica, ou via linha de comando, e manipular os dados diretamente no banco, seja inserindo novos dados, editando ou removendo.  

Os exemplos de _SGBD_ são: MySQL Workbench, voltado para o MySQL, PgAdmin, voltado para o PostgreSQL, SSMS, voltado para o SQL SERVER (Microssoft), o DB Manager, voltado para o SQL, DBeaver, que pode ser utilizado na maioria desses dbs que citei acima.

Como eu já estou contando que já tenha o [Qgis](www.qgis.org) instalado na máquina, não tenho a intenção de utilizar algum SGDB citado acima.  

## Vantagens e desvantagens de se utilizar um db

Os dbs possuem um ótimo motivo para serem utilizados, eles são ótimos para guardar grandes volumes de dados.

> O SHAPEFILE aguenta guardar volumes de até 2 gigas, enquanto o GEOPACKAGE pode guardar aproximadamente 140 TERABYTES de informação [de acordo com a sua documentação](https://www.geopackage.org/spec130/index.html) (dê uma olhada no item 1.1.1.1.1, Requirement 2)

Outra vantagem em usar um db é a questão de velocidade de acesso/gravação, pois são desenvolvidos para serem utilizados com grandes volumes de dados e precisam de acesso rápido a esses dados, muito mais rápido do que comparados com arquivos tradicionais.  
E mesmo assim há ferramentas nos bancos que permitem acelerar ainda mais esse acesso, como a criação de índices, (é mais fácil procurar pela palavra "exemplo" começando a busca a partir de verbetes que iniciam com "ex" do que buscando a partir do "a" não é mesmo?)  
Criação de tabelas em locais onde a leitura e gravação é mais rápida, como em SSDs, e até mesmo na memória RAM (há bancos que guardam dados apenas nessa memória, sendo necessária alguma técnica para persistência dos dados).

Linguagem própria e padronizada para a manipulação dos dados.  
Isso ajuda muito pois a diferença de sintaxe é bem muito pequena de um banco SQL para outro, pois há uma padronização de palavras chaves utilizadas a ser seguida. O `SELECT` sempre vai ter um comportamento parecido em todos os bancos, facilitando o aprendizado.

Economia de espaço. É MUITO comum vermos nas empresas as pessoas compartilharem dados através de pendrives, pastas compartilhadas na rede, fazendo com que haja inúmeras replicas desse mesmo dados, o que faz com que muito espaço seja destinado para essas duplicatas, imaginem se, no lugar de termos cópias desatualizadas em nossas máquinas, tivéssemos o acesso a um local com todos esses arquivos para consulta, edição... seria legal né?!  
Assim, no lugar de termos gigas dos HDs e SSDs destinados para armazenamentos de planilhas e outros arquivos, poderiamos term uma máquina apenas com um banco e acesso a esses dados.  
Em alguns casos o armazenamento de dados em dbs ocupa menos espaço do que em arquivos separados em máquinas individuais.

Mas e se for necessário mais de uma pessoa ter acesso a esse dado?  Não se preocupe, a maioria desses bancos suportam o acesso multi usuário, tanto para edição quanto para leitura, bastando ter as permissões corretas para isso. Infelizmente, nosso amiguinho [SQLITE](https://www.sqlite.org) não é bom com acessos de vários usuários.

Automação de tarefas.  
Pensa no seguinte, sempre que coloca um polígono em um arquivo, vc tem de calcular o perímetro, a área e a quantidade de vértices, verificar se há alguns erros de geometria, sobreposição com outros polígonos. Imagina a dificuldade de fazer isso manualmente para  uma quantidade grande de dados, como um cadastro multifinalitário, de lotes, de propriedades rurais.  
Um db é apenas mais uma ferramenta que possibilita fazer esses cálculos e verificações.

Grande variedade de tipos de dados. Por terem nascidos para receberem dados, com o passar do tempo, as pessoas foram percebendo que era necessário ter suporte a uma infinidade de dados e estruturas de dados que vão ajudar a organizar e trabalhar melhor o nosso trabalho no dia a dia.  
Por exemplo, o excell, muito utilizado como banco de dados, suas células permite a inserção de qualquer tipo de dado em suas células, mas e se quisermos que em determinado campo só seja aceito números, e não letras, datas, pontos, linhas, polígonos. E há inúmeros outros tipos de dados suportados, vai variar de banco para banco. E em alguns, podemos criar o nosso próprio tipo de dado.

O que pode ser uma vantagem, também se torna uma desvantágem, pois é necessário aprender uma linguagem de programação, e tais _palavrinhas_ causam muitos bloqueios na mente de muitas pessoas por aí, e receios também.

Nem sempre podemos carregar os dados dos bancos com a gente.  
Como já falado anteriormente, a maioria dos bancos precisam ser instalados em uma máquina para serem utilizados, seja a própria máquina do utilizador, um servidor próprio para isso ou uma máquina qualquer que possa ser acessada pela rede intranet, por exemplo.
